This directory contains tests written in Bourne Shell. As
such, it is not integrated with Cargo, because I don't know
how. The debug version of Wooden Pieces must be built with
Cargo in the usual way before running the tests.

Currently, just "sh genmoves-test.sh" is supported. It will
print differences between the output of the Wooden Pieces
move generator and the output of an older Minichess program
I wrote in Java. There are 100 random positions resulting
from search in a depth-8 negamax-ab self-game, and a few
handcrafted positions.
