#!/bin/sh
# Copyright © 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.

TESTDIR=/tmp/genmoves-tests
rm -rf $TESTDIR
mkdir $TESTDIR
for i in genmoves-tests/*.in
do
    OUT=`basename $i .in`.out
    ../target/debug/woodpieces --genmoves --posn <$i >$TESTDIR/$OUT
    diff genmoves-tests/$OUT $TESTDIR/$OUT
done
