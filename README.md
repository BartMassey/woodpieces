# Wood Pieces.
Copyright (c) 2017 Bart Massey

Wood Pieces is a Rustic
[Minichess](http://wiki.cs.pdx.edu/minichess) AI player.  It
is being actively implemented as part of Bart's Portland
State University CS 442/542 course on Adversary Search.

## Current Status

This is a work in progress.

* Performs fixed-depth negamax alpha-beta pruned search.
* Plays on the Internet Minichess Server.
* Can do fixed-timer iterative deepening.
* Has Transposition Tables with move ordering.
* Chooses shortest-win longest-loss moves.

Still to do:

* Opening Book
* Endgame Database

Will possibly be done:

* Improved heuristic
* Heuristic time management
* Think on opponent's time
* Search Extensions
* Zero-window search

---

This program is licensed under the "MIT License".
Please see the file COPYING in the source
distribution of this software for license terms.
