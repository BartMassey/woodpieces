// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Wood Pieces is a Rustic Minichess program
//! designed both for reasonable efficiency and
//! tutorial value.

#[macro_use]
pub mod game;
pub use crate::game::*;

use anyhow::{anyhow, Context};
use getopts::{Matches, Options};

use std::env;
use std::fs::File;
use std::process::exit;

/// Play a game, displaying progress. Return the game
/// result.
fn play_game(
    posn: &mut Posn,
    white: &mut Box<dyn Player>,
    black: &mut Box<dyn Player>,
) -> anyhow::Result<Option<Color>> {
    // XXX Apparently since result is provably only assigned
    // once, the compiler does not need it to be
    // mutable. Which is weird.
    let result;
    let mut on_move;
    // Make and display moves until the game is over.
    loop {
        // Display the current position.
        display!("{}", posn);
        // Obtain and display the chosen move.
        on_move = posn.on_move;
        let selection = match on_move {
            White => white.select_move(posn),
            Black => black.select_move(posn),
        };
        match selection {
            NextMove(chosen_move, opt_value) => {
                if let Some(move_value) = opt_value {
                    displayln!("{} ({})", chosen_move, move_value)
                } else {
                    displayln!("{}", chosen_move)
                }
                displayln!();
                // Report the move to the other side.
                // Usually it will be ignored.
                match on_move {
                    White => black.report_move(posn, chosen_move),
                    Black => white.report_move(posn, chosen_move),
                };
                // Make the move and return the win or draw if
                // the game is over.
                let (move_result, _) = posn.make_move(&chosen_move);
                match move_result {
                    Playing(_) => (),
                    final_result => {
                        result = final_result;
                        break;
                    }
                };
            }
            GameOver(value) => {
                result = value;
                displayln!();
                break;
            }
        };
    }
    displayln!("{}", posn);
    match result {
        Win(_) => Ok(Some(on_move)),
        Loss(_) => Ok(Some(on_move.opponent())),
        Draw => Ok(None),
        Playing(_) => Err(anyhow!("game stopped early")),
    }
}

/// Construct a player for the given color with the given description.
/// XXX If the analyzer, adjust the depth.
fn get_player(name: &str, color: Color, analyze: bool) -> Option<Box<dyn Player>> {
    let desc: Vec<&str> = name.split(':').collect();
    let get_depth = || {
        if desc.len() != 2 {
            return None;
        };
        let depth = match desc[1].parse::<usize>() {
            Ok(n) => {
                if analyze {
                    n - 1
                } else {
                    n
                }
            }
            Err(_) => return None,
        };
        if depth == 0 {
            eprintln!("depth must be positive");
            return None;
        };
        Some(depth)
    };
    fn parse_remote<'a>(desc: &'a [&'a str]) -> Option<(&'a str, &'a str, &'a str, &'a str)> {
        if desc.is_empty() {
            return None;
        };
        let mut result = Vec::new();
        let defaults = ["", "woodpieces", "imcs.svcs.cs.pdx.edu", "3589"];
        for i in 0..4 {
            if desc.len() > i {
                result.push(desc[i])
            } else {
                result.push(defaults[i])
            }
        }
        Some((result[0], result[1], result[2], result[3]))
    }
    match desc[0] {
        "human" => Some(Box::new(HumanPlayer::new())),
        "random" => Some(Box::new(RandomPlayer::new())),
        "heuristic" => Some(Box::new(NegamaxPlayer::new(1, false, color, None))),
        "negamax" | "ab" => {
            if let Some(depth) = get_depth() {
                let ab = desc[0] == "ab";
                let tt = TTable::boxed();
                Some(Box::new(NegamaxPlayer::new(depth, ab, color, tt)))
            } else {
                None
            }
        }
        "id" => {
            if desc.len() != 2 {
                return None;
            };
            let time = match desc[1].parse::<f32>() {
                Ok(t) => t,
                Err(_) => return None,
            };
            let tt = TTable::boxed();
            Some(Box::new(IDPlayer::new(Some(time), true, color, tt)))
        }
        "offer" => {
            if desc.len() < 2 {
                return None;
            };
            if let Some((pass, name, host, port)) = parse_remote(&desc[1..]) {
                Some(Box::new(IMCSPlayer::offer(
                    name,
                    pass,
                    host,
                    port,
                    color.opponent(),
                )))
            } else {
                None
            }
        }
        "accept" => {
            if desc.len() < 3 {
                return None;
            };
            let game_id = desc[1];
            if let Some((pass, name, host, port)) = parse_remote(&desc[2..]) {
                Some(Box::new(IMCSPlayer::accept(
                    name,
                    pass,
                    host,
                    port,
                    color.opponent(),
                    game_id,
                )))
            } else {
                None
            }
        }
        _ => None,
    }
}

/// Dig the given color's player type out of the arguments
/// and construct the player.
fn select_player(opts: &Options, args: &Matches, color_arg: &str) -> Box<dyn Player> {
    let color = match color_arg {
        "white" => White,
        "black" => Black,
        _ => panic!("bad color {}", color_arg),
    };
    let name = match args.opt_str(color_arg) {
        Some(s) => s,
        None => usage(opts),
    };
    match get_player(&name, color, false) {
        Some(p) => p,
        None => usage(opts),
    }
}

/// Run a game and display the game result.
fn run_game(posn: &mut Posn, opts: &Options, args: &Matches) -> anyhow::Result<()> {
    let mut white = select_player(opts, args, "white");
    let mut black = select_player(opts, args, "black");
    let result = play_game(posn, &mut white, &mut black)?;
    match result {
        Some(color) => displayln!("{} wins", color),
        None => displayln!("draw"),
    }
    Ok(())
}

/// Display the moves for the current position.
fn run_genmoves(posn: &mut Posn) {
    let mut moves = posn.movegen();
    moves.sort();
    for m in moves {
        println!("{}", m);
    }
}

fn run_analyze(posn: &mut Posn, opts: &Options, player_str: &str) {
    let mut player = match get_player(player_str, posn.on_move.opponent(), true) {
        Some(p) => p,
        None => usage(opts),
    };
    let mut results = Vec::new();
    let moves = posn.movegen();
    for ref m in moves {
        let (posn_value, undo) = posn.make_move(m);
        let value = match posn_value {
            Playing(_) => match player.select_move(posn) {
                NextMove(_, Some(v)) => v,
                GameOver(v) => v,
                _ => panic!("player did not evaluate move"),
            },
            final_value => final_value.negate(),
        };
        posn.unmake_move(m, &undo);
        results.push((value, *m));
    }
    results.sort();
    print!("{}", posn);
    for (v, m) in results {
        println!("{} ({})", m, v.negate());
    }
}

fn usage(opts: &Options) -> ! {
    let brief = "woodpieces: usage: woodpieces [options]";
    eprint!("{}", opts.usage(brief));
    eprint!(
        "\nAvailable Players:\n    \
                      human\n    \
                      random\n    \
                      heuristic\n    \
                      negamax:<depth>\n    \
                      ab:<depth>\n    \
                      offer:<password>[:username[:host[:port]]]\n    \
                      accept:<game-id>:<password>\
                      [:username[:host[:port]]]\n"
    );
    exit(1);
}

/// Process command-line arguments and proceed as instructed.
fn run() -> anyhow::Result<()> {
    // Set up the RNG.
    myrng::seed();
    // Set up the ttable hash data.
    zobrist::fill_table();
    // Parse arguments.
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optopt("", "white", "white player", "PLAYER");
    opts.optopt("", "black", "black player", "PLAYER");
    opts.optopt(
        "",
        "analyze",
        "generate moves and analysis for position and exit",
        "PLAYER",
    );
    opts.optopt("", "posn", "read initial position", "FILE");
    opts.optflag("", "quiet", "do not display game progress");
    opts.optopt(
        "",
        "ttable",
        "use transposition table of size 2**SIZE",
        "SIZE",
    );
    opts.optflag("", "genmoves", "generate moves for position and exit");
    opts.optflag("", "check-eval", "check incremental eval against full");
    opts.optflag("", "check-ab", "check ab-pruned search against full");
    opts.optflag("", "shuffle", "shuffle moves in ab");
    opts.optflag(
        "",
        "no-shuffle-top",
        "do not shuffle moves to be returned in negamax",
    );
    opts.optflag("", "no-hash", "do not maintain hash (turns off ttable)");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            eprintln!("{}", f);
            usage(&opts);
        }
    };

    // Shut the program up when called for.
    if matches.opt_present("quiet") {
        unsafe { DISPLAY_GAME = false };
    };

    // Turn on checking of incremental eval when asked for.
    if matches.opt_present("check-eval") {
        unsafe { CHECK_INCREMENTAL_EVAL = true };
    };

    // Turn on checking of ab when asked for.
    if matches.opt_present("check-ab") {
        unsafe { CHECK_AB = true };
    };

    // Turn off move shuffling in ab when asked for.
    if matches.opt_present("shuffle") {
        unsafe { SHUFFLE_MOVES = true };
    };

    // Turn off top-level move shuffling in negamax when asked for.
    if matches.opt_present("no-shuffle-top") {
        unsafe { SHUFFLE_TOP = false };
    };

    // Turn off ttables if desired.
    if let Some(lg_size_str) = matches.opt_str("ttable") {
        let lg_size = match lg_size_str.parse() {
            Ok(s) => s,
            Err(_) => usage(&opts),
        };
        unsafe { TTABLE_LG_SIZE = lg_size };
    };

    // Turn off hashing if desired. This also turns off
    // ttables, by necessity.
    if matches.opt_present("no-hash") {
        unsafe { TTABLE_LG_SIZE = 0 };
        unsafe { USE_HASH = false };
    };

    // Get initial position.
    let mut posn = if let Some(posn_file) = matches.opt_str("posn") {
        let mut f = File::open(posn_file).context("could not open position file")?;
        Posn::read(&mut f).context("could not read position")?
    } else {
        Posn::initial()
    };

    // Analyze a position.
    if let Some(player_str) = matches.opt_str("analyze") {
        unsafe { DISPLAY_GAME = false };
        run_analyze(&mut posn, &opts, &player_str);
        return Ok(());
    }

    // Test the move generator.
    if matches.opt_present("genmoves") {
        run_genmoves(&mut posn);
        return Ok(());
    };

    // Play a game.
    run_game(&mut posn, &opts, &matches)
}

fn main() {
    if let Err(e) = run() {
        println!("woodpieces: {}", e);
        std::process::exit(1);
    }
}
