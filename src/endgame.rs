// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.


//! Build MiniChess endgame tablebases.
//! 
//! This fragile piece of experimental code will build an
//! endgame tablebase for the collection of white and black
//! pieces specified on the command line. The tablebase
//! will be for white always on move: the board can be flipped
//! for black.
//! 
//! This code requires that the endgame tablebases for the
//! subcollections that this one can lead into be built.
//! 
//! The code knows that capturing a king ends the game, so
//! there is no need for a single-king tablebase. Start by
//! building the two-kings tablebase.

#[macro_use]
pub mod game;
pub use game::*;

extern crate multiset;
use multiset::*;

use std::collections::*;
#[cfg(test)]
use std::iter::FromIterator;
use std::env::args;
use std::io::{Write, stderr};
use std::process::exit;

impl Board {
    /// Flip the board and change the piece colors to get
    /// an equivalent board with the other side on move.
    pub fn flip(&mut self) {

        fn flip_color(s: Square) -> Square {
            match s {
                Empty => Empty,
                Full(Piece{color, rank}) =>
                    Full(Piece{color: color.opponent(), rank: rank})
            }
        }

        // XXX This code only works for boards with even
        // number of rows.
        assert!(NROWS & 1 == 0);
        for r in 0..NROWS / 2 {
            // Match row 0 with row 5, etc...
            let xr = NROWS - r - 1;
            for c in 0..NCOLS {
                let p = self.squares[r][c];
                let xp = self.squares[xr][c];
                self.squares[r][c] = flip_color(xp);
                self.squares[xr][c] = flip_color(p);
            };
        };
    }

    pub fn pieces(&self) -> HashMultiSet<Piece> {
        let mut result = HashMultiSet::new();
        for r in 0..NROWS {
            for c in 0..NCOLS {
                if let Full(piece) = self.squares[r][c] {
                    result.insert(piece)
                }
            }
        };
        result
    }

    /// Insert all the subpositions created by
    /// placing all pieces in the given collection
    /// at all possible positions on the board.
    fn place_pieces(&mut self, pieces: &mut HashMultiSet<Piece>,
                    posns: &mut HashSet<Board>) {
        // Base case: if no pieces remain, we save a position.
        let next_piece = match pieces.distinct_elements().next() {
            None => {
                assert!(!posns.contains(self));
                posns.insert(self.clone());
                return;
            },
            Some(piece) => *piece
        };
        // Recursive case: put the piece everywhere. If
        // we run into another of the same piece, we end
        // the loop to take things in lexicographic order
        // and break symmetries.
        pieces.remove(next_piece);
        for r in 0..NROWS {
            for c in 0..NCOLS {
                match self.squares[r][c] {
                    Empty => {
                        self.squares[r][c] = Full(next_piece);
                        self.place_pieces(pieces, posns);
                        self.squares[r][c] = Empty;
                    },
                    Full(piece) => {
                        if piece == next_piece {
                            pieces.insert(next_piece);
                            return;
                        }
                    }
                }
            }
        };
        pieces.insert(next_piece);
    }
}

/// Given a collection of pieces to place, place each of
/// them in all possible locations, and return the resulting
/// set of all positions. Care is taken to avoid repeating
/// positions through piece symmetry: although the set would
/// take care of it we don't want to spend the time.
fn place_all(pieces: HashMultiSet<Piece>) -> HashSet<Board> {
    let mut board = Board::empty();
    let mut pieces = pieces.clone();
    let mut posns = HashSet::new();
    board.place_pieces(&mut pieces, &mut posns);
    posns
}

/// Test that we get the right number of placements
/// in an interesting case.
#[test]
fn place_all_count() {
    fn place_and_check(pieces: Vec<Piece>, count: usize) {
        let pieces: HashMultiSet<Piece> =
            FromIterator::from_iter(pieces);
        let posns = place_all(pieces);
        assert!(posns.len() == count);
    }

    // 30 * 29 / 2 P placements, 28 K placements
    let pieces = vec![
        Piece{color: White, rank: King},
        Piece{color: White, rank: Pawn},
        Piece{color: White, rank: Pawn}];
    place_and_check(pieces, 15 * 29 * 28);
    // Order shouldn't matter
    let pieces = vec![
        Piece{color: White, rank: Pawn},
        Piece{color: White, rank: King},
        Piece{color: White, rank: Pawn}];
    place_and_check(pieces, 15 * 29 * 28);
}

/// Report bad usage and exit.
fn usage() -> ! {
    writeln!(stderr(), "endgame: usage: endgame <piece-list>")
        .expect("error writing usage");
    exit(1);
}

/// Turn the given piece description string
/// into a collection of pieces containing exactly
/// one K and one k.
fn make_pieces(desc: &str) -> HashMultiSet<Piece> {
    let mut result = HashMultiSet::new();
    for piece_char in desc.chars() {
        let piece = match piece_char.to_string().parse() {
            Ok(c) => c,
            Err(_) => usage()
        };
        result.insert(piece);
    };
    for piece_char in ['K', 'k'].iter() {
        let piece = piece_char.to_string().parse().unwrap();
        let n = result.count_of(piece);
        if n > 1 {
            usage();
        };
        if n == 0 {
            result.insert(piece)
        };
    };
    result
}

#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum BoardValue {
    Loss(usize),
    Win(usize)
}

/// Build and return an endgame tablebase for the given
/// collection of pieces.
fn build_tablebase(pieces: HashMultiSet<Piece>) -> HashMap<Board, BoardValue> {
    // XXX For now, just the Kk case.
    assert!(pieces.total_elements() == 2);
    let mut tb = HashMap::new();
    let mut open = place_all(pieces);
    while !open.is_empty() {
        let mut next_open = HashSet::new();
        let close_state = |s| {
            for um in s.gen_unmoves() {
                let mut us = s.clone();
                us.make_unmove(um);
                us.flip();
                if !tb.contains_key(&us) {
                    next_open.insert(us)
                };
            };
        };
        for s in open {
            assert!(!tb.contains_key(&s));
            let moves = s.genmoves();
            if moves.len() == 0 {
                tb.insert(s, BoardValue::Loss(0));
                close_state(s);
                continue;
            };
            let mut loss = true;
            let mut max_loss = None;
            for m in &s.genmoves() {
                let mut ms = s.clone();
                let undo = ms.make_move(White, 1, m);
                ms.flip();
                if undo.old_dest != Empty {
                    // XXX deal with captures.
                    panic!("no endgame captures yet");
                }
                if tb.contains_key(&ms) {
                    
                }
            }
        }
    };
    tb
}

pub fn main() {
    let argv: Vec<String> = args().collect();
    if argv.len() != 2 {
        usage();
    };
    let pieces = make_pieces(&argv[1]);
    let posns = place_all(pieces);
    println!("{}", posns.len());
}
