// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Move generation and execution.

use crate::board::*;
use crate::eval::*;
use crate::posn::*;
use std::cmp::Ordering;
use std::fmt;
use std::str::FromStr;

/// When true, verify that incremental eval gives the same
/// answer as full eval (slow).
pub static mut CHECK_INCREMENTAL_EVAL: bool = false;

/// Move parsing error.
#[derive(Debug)]
pub struct ParseMoveError(String);

/// Location coordinates on the board.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Loc {
    /// Row.
    pub r: usize,
    /// Column.
    pub c: usize,
}

impl Loc {
    pub fn coords(&self) -> (usize, usize) {
        (self.r, self.c)
    }
}

impl fmt::Display for Loc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        assert!(self.r < NROWS && self.c < NCOLS);
        let col = (self.c + 'a' as usize) as u8 as char;
        write!(f, "{}{}", col, self.r + 1)
    }
}

impl FromStr for Loc {
    type Err = ParseMoveError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Grab the row and column numbers.
        let schars = s.as_bytes();
        if schars.len() != 2 {
            return Err(ParseMoveError(format!("location not 2 chars '{}'", s)));
        };
        let col = schars[0] as isize - 'a' as isize;
        let row = schars[1] as isize - '1' as isize;
        // Check that everything is valid.
        if col < 0 || col >= NCOLS as isize || row < 0 || row >= NROWS as isize {
            return Err(ParseMoveError(format!("invalid location '{}'", s)));
        };
        // Return the new location.
        Ok(Loc {
            r: row as usize,
            c: col as usize,
        })
    }
}

/// We want to order locations algebraically, which is
/// backward from the derived order.
impl Ord for Loc {
    fn cmp(&self, other: &Self) -> Ordering {
        let Loc { r: rs, c: cs } = *self;
        let Loc { r: ro, c: co } = *other;
        (cs, rs).cmp(&(co, ro))
    }
}

impl PartialOrd for Loc {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct Move {
    /// Start location of move.
    pub start: Loc,
    /// End location of move.
    pub end: Loc,
}

impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{}", self.start, self.end)
    }
}

impl FromStr for Move {
    type Err = ParseMoveError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Split the move string into two location strings.
        let mut locs = s.split('-');
        let start = match locs.next() {
            Some(l) => l.parse()?,
            None => return Err(ParseMoveError("move start missing".to_string())),
        };
        let end = match locs.next() {
            Some(l) => l.parse()?,
            None => return Err(ParseMoveError("move end missing".to_string())),
        };
        if locs.count() != 0 {
            return Err(ParseMoveError("garbage at end of move".to_string()));
        };
        // Return the resulting move.
        Ok(Move { start, end })
    }
}

impl Move {
    /// Convenience function: Make a new move directly
    /// from starting and ending coordinates.
    pub fn new(r_start: usize, c_start: usize, r_end: usize, c_end: usize) -> Move {
        Move {
            start: Loc {
                r: r_start,
                c: c_start,
            },
            end: Loc { r: r_end, c: c_end },
        }
    }

    pub fn coords(&self) -> ((usize, usize), (usize, usize)) {
        (self.start.coords(), self.end.coords())
    }
}

/// Capture flag values for scanner.
#[derive(Clone, Copy, PartialEq, Eq)]
enum Capture {
    /// Capture is allowed but not required.
    Yes,
    /// Capture is not allowed.
    No,
    /// Non-capture is not allowed.
    Only,
}

/// Move undo information for `unmake_move()`.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Undo {
    /// Prior source square.
    source: Square,
    /// Prior destination square.
    dest: Square,
    /// Prior position value. Overloaded in `Board::make_move()`
    /// to communicate move result.
    value: PosnValue,
    /// Prior hash.
    hash: u64,
}

impl Board {
    /// Move the piece sitting at `r0`,`c0` along the vector
    /// given by `dr`, `dc`, marking each possible move thus
    /// obtained. Stop when walking off the board, after one
    /// step if `one_step` is true, or after dealing with
    /// possible captures depending on `capture_mode`.
    /// Found moves are added to the `moves` list.
    #[allow(clippy::too_many_arguments)]
    fn scan(
        &self,
        on_move: Color,
        moves: &mut Vec<Move>,
        r0: usize,
        c0: usize,
        dr: isize,
        dc: isize,
        one_step: bool,
        capture_mode: Capture,
    ) {
        // Ensure moving one of the right color pieces.
        assert!(self.squares[r0][c0].color() == Some(on_move));
        // Set up the destination location markers.
        let mut r = r0 as isize;
        let mut c = c0 as isize;
        loop {
            // Move destination as specified.
            r += dr;
            c += dc;
            // Stop if walked off the board.
            if r < 0 || r >= NROWS as isize || c < 0 || c >= NCOLS as isize {
                break;
            };
            // Get the target square.
            let t = self.squares[r as usize][c as usize];
            // Check whether the move is legal.
            let do_move = match t {
                Empty => capture_mode != Capture::Only,
                Full(p) => p.color == on_move.opponent() && capture_mode != Capture::No,
            };
            // If the move is legal, save it. Otherwise stop.
            if do_move {
                moves.push(Move::new(r0, c0, r as usize, c as usize))
            }
            // If we have made our one move or run into something, break.
            if one_step || t != Empty {
                break;
            };
        }
    }

    // Scan in the given direction, then rotate three times
    // by 90 degrees and scan again. See `scan()`.
    #[allow(clippy::too_many_arguments)]
    fn symscan(
        &self,
        on_move: Color,
        moves: &mut Vec<Move>,
        r0: usize,
        c0: usize,
        dr: isize,
        dc: isize,
        one_step: bool,
        capture_mode: Capture,
    ) {
        let mut next_dr = dr;
        let mut next_dc = dc;
        for _ in 0..4 {
            self.scan(
                on_move,
                moves,
                r0,
                c0,
                next_dr,
                next_dc,
                one_step,
                capture_mode,
            );
            // This does the 90 degree turn.
            let tmp = next_dr;
            next_dr = next_dc;
            next_dc = -tmp;
        }
    }

    /// Generate and return all the legal moves from the
    /// given position. XXX This should be an iterator, but
    /// that's really hard.
    pub fn movegen(&self, on_move: Color) -> Vec<Move> {
        let mut moves = Vec::new();
        // Scan the board for correct-colored pieces to move.
        // When one is found, add its legal moves to the list.
        for r in 0..NROWS {
            for c in 0..NCOLS {
                let s = self.squares[r][c];
                match s {
                    Empty => continue,
                    Full(piece) => {
                        if piece.color == on_move {
                            match piece.rank {
                                King => {
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        0,
                                        true,
                                        Capture::Yes,
                                    );
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        1,
                                        true,
                                        Capture::Yes,
                                    );
                                }
                                Queen => {
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        0,
                                        false,
                                        Capture::Yes,
                                    );
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        1,
                                        false,
                                        Capture::Yes,
                                    );
                                }
                                Rook => {
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        0,
                                        false,
                                        Capture::Yes,
                                    );
                                }
                                Bishop => {
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        1,
                                        false,
                                        Capture::Yes,
                                    );
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        1,
                                        0,
                                        true,
                                        Capture::No,
                                    );
                                }
                                Knight => {
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        2,
                                        -1,
                                        true,
                                        Capture::Yes,
                                    );
                                    self.symscan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        2,
                                        1,
                                        true,
                                        Capture::Yes,
                                    );
                                }
                                Pawn => {
                                    let dirn = match on_move {
                                        White => 1,
                                        Black => -1,
                                    };
                                    self.scan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        dirn,
                                        0,
                                        true,
                                        Capture::No,
                                    );
                                    self.scan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        dirn,
                                        -1,
                                        true,
                                        Capture::Only,
                                    );
                                    self.scan(
                                        on_move,
                                        &mut moves,
                                        r,
                                        c,
                                        dirn,
                                        1,
                                        true,
                                        Capture::Only,
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
        moves
    }

    /// Make the given move on the given board. Return the
    /// resulting state: a `PosnValue` of `Playing`(d)
    /// indicates a delta of d should be applied to the
    /// current value of the position.
    pub fn make_move(&mut self, on_move: Color, move_num: usize, m: &Move, hash: u64) -> Undo {
        let ((rs, cs), (re, ce)) = m.coords();
        // Save information for update and result.
        let old_source = self.squares[rs][cs];
        let old_dest = self.squares[re][ce];
        let mut dest = old_source;
        assert!(old_source.color() == Some(on_move));
        // Handle pawn promotion.
        if let Full(Piece {
            rank: Pawn,
            color: source_color,
        }) = old_source
        {
            if (source_color == White && re == NROWS - 1) || (source_color == Black && re == 0) {
                dest = Full(Piece {
                    rank: Queen,
                    color: source_color,
                });
            };
        };
        // Handle win by king capture, draw by number of
        // moves, and update of the position value.
        let result = if let Full(Piece {
            rank: King,
            color: dest_color,
        }) = old_dest
        {
            assert!(dest_color == on_move.opponent());
            Win(0)
        } else if on_move == Black && move_num >= GAME_LENGTH {
            Draw
        } else {
            Playing(PosnValue::adjustment(dest, old_source, old_dest))
        };
        // Update the board and return the necessary
        // information.
        let new_hash = if unsafe { USE_HASH } {
            self.zobrist_move_hash(hash, &m.start, &m.end, dest)
        } else {
            0
        };
        self.squares[rs][cs] = Empty;
        self.squares[re][ce] = dest;
        Undo {
            source: old_source,
            dest: old_dest,
            value: result,
            hash: new_hash,
        }
    }

    pub fn unmake_move(&mut self, m: &Move, putback: &Undo) {
        // Destructure the move and undo information.
        let ((rs, cs), (re, ce)) = m.coords();
        // Put the source and destination squares back.
        self.squares[rs][cs] = putback.source;
        self.squares[re][ce] = putback.dest;
    }
}

impl Posn {
    /// Find the moves on the board for this position.
    pub fn movegen(&self) -> Vec<Move> {
        self.board.movegen(self.on_move)
    }

    /// Apply the given move to the position.
    pub fn make_move(&mut self, m: &Move) -> (PosnValue, Undo) {
        let putback = self
            .board
            .make_move(self.on_move, self.move_num, m, self.hash);
        let undo = Undo {
            source: putback.source,
            dest: putback.dest,
            value: self.posn_value,
            hash: self.hash,
        };
        self.hash = if unsafe { USE_HASH } {
            self.zobrist_side_change_hash(putback.hash)
        } else {
            0
        };
        // Adjust the position value as needed.
        let adj_v = match putback.value {
            Playing(v) => {
                if let Playing(ov) = self.posn_value {
                    Playing(ov + v)
                } else {
                    panic!("move played after final position")
                }
            }
            r => r,
        };
        // We're changing sides, so negate.
        self.posn_value = adj_v.negate();
        // Flip the color and update the move number.
        match self.on_move {
            White => {
                self.on_move = Black;
            }
            Black => {
                self.on_move = White;
                self.move_num += 1;
            }
        };
        if unsafe { CHECK_INCREMENTAL_EVAL } {
            let val = self.value();
            if self.posn_value != val {
                panic!("value {} eval {}\n{}", self.posn_value, val, self);
            };
        };
        (putback.value, undo)
    }

    /// Undo the given move using its coordinates and
    /// supplied undo information.
    pub fn unmake_move(&mut self, m: &Move, putback: &Undo) {
        // Undo the board.
        self.board.unmake_move(m, putback);
        // Repair the position value.
        self.posn_value = putback.value;
        self.hash = putback.hash;
        // Flip the side back and undo the move number
        // update.
        match self.on_move {
            White => {
                self.on_move = Black;
                self.move_num -= 1;
                assert!(self.move_num > 0);
            }
            Black => {
                self.on_move = White;
            }
        }
    }
}
