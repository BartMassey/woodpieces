// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

//! Manage MiniChess boards. A board is represented as a
//! two-dimensional array of squares in row-column order.
//!
//! For efficiency and convenience reasons, a MiniChess
//! player `Posn` represents the board as a raw array rather
//! than a `Board` structure. Conversions are offered here
//! to avoid duplicate code.

use std::fmt;
use std::str::FromStr;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum DescriptionError {
    #[error("piece not 1 char '{0}'")]
    ErrPieceMultipleChars(String),
    #[error("bad color '{0}'")]
    ErrBadColor(String),
    #[error("bad rank '{0}'")]
    ErrBadRank(char),
    #[error("not a piece '{0}'")]
    ErrNotAPiece(String),
    #[error("bad row {0} in board")]
    ErrBadBoardRow(String),
    #[error("bad row count {0} for board")]
    ErrBadBoardRowCount(usize),
    #[error("bad info line for position:\n{0}")]
    ErrBadInfoLine(String),
    #[error("missing move number for position")]
    ErrMissingMoveNumber,
    #[error("missing on-move player for position")]
    ErrMissingOnMove,
    #[error("could not read position file: {0}")]
    ErrBadPosnFile(#[from] std::io::Error),
    #[error("invalid UTF-8 in position file: {0}")]
    ErrBadPosnFileUtf8(#[from] std::str::Utf8Error),
    #[error("invalid move number: {0}")]
    ErrInvalidMoveNumber(#[from] std::num::ParseIntError),
}
pub use DescriptionError::*;

/// Squares.
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Square {
    Empty,
    Full(Piece),
}
pub use self::Square::*;

impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Empty => write!(f, "."),
            Full(piece) => write!(f, "{}", piece),
        }
    }
}

impl FromStr for Square {
    type Err = DescriptionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let schars = s.as_bytes();
        if schars.len() != 1 {
            return Err(ErrPieceMultipleChars(s.into()));
        };
        let c = schars[0] as char;
        match c {
            '.' => Ok(Empty),
            _ => Ok(Full(s.parse()?)),
        }
    }
}

impl Square {
    /// Return the color of the piece in the square, if any.
    pub fn color(self) -> Option<Color> {
        match self {
            Empty => None,
            Full(p) => Some(p.color),
        }
    }

    /// Return the rank of the piece in the square, if any.
    pub fn rank(self) -> Option<Rank> {
        match self {
            Empty => None,
            Full(p) => Some(p.rank),
        }
    }
}

/// Sides / colors. It is important that `White` is first,
/// since White moves first and this is exploited for array
/// indexing.
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum Color {
    White,
    Black,
}
pub use self::Color::*;

// XXX Rust has no way to let us derive this.

/// Number of colors.
pub const NCOLORS: usize = 2;

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let color = match *self {
            White => 'W',
            Black => 'B',
        };
        write!(f, "{}", color)
    }
}

impl FromStr for Color {
    type Err = DescriptionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "W" => Ok(White),
            "B" => Ok(Black),
            _ => Err(ErrBadColor(s.into())),
        }
    }
}

impl Color {
    /// Return the "opposite" of the given color.
    pub fn opponent(self) -> Color {
        match self {
            White => Black,
            Black => White,
        }
    }
}

/// Return the uppercase version of a lowercase ASCII character,
/// leaving other characters alone. XXX Rust hates ASCII.
fn uppercase(c: char) -> char {
    if ('a'..='z').contains(&c) {
        return ((c as isize) - ('a' as isize) + ('A' as isize)) as u8 as char;
    };
    c
}

/// Return the lowercase version of an uppercase ASCII character,
/// leaving other characters alone. XXX Rust hates ASCII.
fn lowercase(c: char) -> char {
    if ('A'..='Z').contains(&c) {
        return ((c as isize) - ('A' as isize) + ('a' as isize)) as u8 as char;
    };
    c
}

/// Ranks.
#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Rank {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}
pub use self::Rank::*;

// XXX This bletchery is because Rust does not allow
// directly iterating over the elements of a simple enum,
// or computing the number of them, etc.
//
// This is *bad*, but there's nothing obvious to be done.

/// Number of ranks.
pub const NRANKS: usize = 6;

/// List of all the ranks.
const RANKS: [Rank; NRANKS] = [Pawn, Knight, Bishop, Rook, Queen, King];

/// The names of the ranks.
const RANK_CHARS: [char; NRANKS] = ['p', 'n', 'b', 'r', 'q', 'k'];

impl Rank {
    // See http://stackoverflow.com/a/21376984/364875 for
    // how to build an iterator over Ranks.

    /// Get the rank described by the given character.
    fn from_char(c0: char) -> Result<Rank, DescriptionError> {
        let c = lowercase(c0);
        // Get and return the piece.
        for i in 0..RANK_CHARS.len() {
            if RANK_CHARS[i] == c {
                return Ok(RANKS[i]);
            }
        }
        Err(ErrBadRank(c0))
    }
}

/// Pieces.
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Piece {
    pub color: Color,
    pub rank: Rank,
}

/// Produce a piece string
impl fmt::Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let rank_char = RANK_CHARS[self.rank as usize];
        let colored_piece = match self.color {
            White => uppercase(rank_char),
            Black => rank_char,
        };
        write!(f, "{}", colored_piece)
    }
}

impl FromStr for Piece {
    type Err = DescriptionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Get the piece character.
        let schars = s.as_bytes();
        if schars.len() != 1 {
            return Err(ErrPieceMultipleChars(s.into()));
        };
        let c = schars[0] as char;
        // Get the rank of the piece.
        let rank = Rank::from_char(c)?;
        // Lowercase is Black, uppercase is White.
        if ('a'..='z').contains(&c) {
            return Ok(Piece { color: Black, rank });
        } else if ('A'..='Z').contains(&c) {
            return Ok(Piece { color: White, rank });
        };
        Err(ErrNotAPiece(s.into()))
    }
}

/// Dimensions. We hold these constant.
pub const NROWS: usize = 6;
pub const NCOLS: usize = 5;

/// Board array.
pub type BoardArray = [[Square; NCOLS]; NROWS];

/// Board structure.
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Board {
    pub squares: BoardArray,
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for rs in self.squares.iter().rev() {
            for s in rs.iter() {
                write!(f, "{}", s)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl FromStr for Board {
    type Err = DescriptionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Get a blank board array.
        let mut board = Board {
            squares: [[Empty; NCOLS]; NROWS],
        };
        // Split the board into rows.
        let mut rows = s.split('\n');
        // Process the board rows.
        let mut r = 0;
        let mut c;
        while r < NROWS {
            let row = match rows.next() {
                Some(s) => s,
                None => break,
            };
            c = 0;
            for s in row.chars() {
                board.squares[NROWS - r - 1][c] = s.to_string().parse()?;
                c += 1;
            }
            if c != NCOLS {
                return Err(ErrBadBoardRow(row.into()));
            };
            r += 1;
        }
        // Check the row count.  XXX There's a blank row at
        // the end because `split()`.
        if r != NROWS || rows.next() != Some("") || rows.next() != None {
            return Err(ErrBadBoardRowCount(r));
        };
        // Return the board.
        Ok(board)
    }
}

impl Board {
    /// Return the initial board for an official game.
    pub fn initial() -> Board {
        let board_str = "kqbnr\n\
           ppppp\n\
           .....\n\
           .....\n\
           PPPPP\n\
           RNBQK\n";
        board_str.parse().expect("invalid initial board")
    }

    /// Return an empty board.
    pub fn empty() -> Board {
        Board {
            squares: [[Empty; NCOLS]; NROWS],
        }
    }
}
