// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Position evaluation, both full and incremental.

use crate::board::*;
use crate::posn::*;

use std::cmp::Ordering;
use std::cmp::Ordering::*;
use std::fmt;

/// Value of current position.
#[derive(Clone, Copy, Debug)]
pub enum PosnValue {
    /// Given side can win in n moves.
    Win(usize),
    /// Given side will lose in n moves.
    Loss(usize),
    /// Game is a definite draw.
    Draw,
    /// Game is still in progress, with given estimated
    /// value.
    Playing(f32),
}
pub use self::PosnValue::*;

impl fmt::Display for PosnValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Win(n) => write!(f, "+ [{}]", n),
            Loss(n) => write!(f, "- [{}]", n),
            Draw => write!(f, "="),
            Playing(val) => write!(f, "{:+.2}", val),
        }
    }
}

impl Ord for PosnValue {
    fn cmp(&self, other: &Self) -> Ordering {
        match *self {
            Win(n) => match *other {
                Win(m) => m.cmp(&n),
                _ => Greater,
            },
            Loss(n) => match *other {
                Loss(m) => n.cmp(&m),
                _ => Less,
            },
            Draw => match *other {
                Win(_) => Less,
                Loss(_) => Greater,
                Draw => Equal,
                Playing(ov) => {
                    if ov == 0.0 {
                        Equal
                    } else if ov < 0.0 {
                        Greater
                    } else {
                        Less
                    }
                }
            },
            Playing(sv) => match *other {
                Win(_) => Less,
                Loss(_) => Greater,
                Draw => {
                    if sv == 0.0 {
                        Equal
                    } else if sv < 0.0 {
                        Less
                    } else {
                        Greater
                    }
                }
                Playing(ov) => {
                    if sv < ov {
                        Less
                    } else if sv > ov {
                        Greater
                    } else {
                        Equal
                    }
                }
            },
        }
    }
}

impl PartialOrd for PosnValue {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[test]
fn posnvalue_ord_tests() {
    let values = [
        Loss(1),
        Loss(2),
        Playing(-1.0),
        Playing(-0.5),
        Draw,
        Playing(0.5),
        Playing(1.0),
        Win(3),
        Win(2),
    ];
    for i in 0..values.len() {
        for j in 0..values.len() {
            if i < j {
                assert!(values[i] < values[j]);
            } else if i > j {
                assert!(values[i] > values[j]);
            } else {
                assert!(values[i] == values[j]);
            }
        }
    }
    for i in 0..values.len() {
        if values[i] < Draw {
            assert!(values[i] < Playing(0.0));
        } else if values[i] > Draw {
            assert!(values[i] > Playing(0.0));
        } else {
            assert!(values[i] == Playing(0.0));
        }
    }
}

impl PartialEq for PosnValue {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Equal
    }
}

impl Eq for PosnValue {}

// XXX This is kind of gross, but Rust doesn't let us index
// an array with the Rank enum directly.

/// Rank values of pieces, in increasing order. The King has
/// no defined rank.
const RANK_VALUES: [f32; NRANKS - 1] = [1.0, 1.5, 3.0, 4.0, 6.0];

impl Piece {
    /// Generic material value of a piece. Should not
    /// be called with a King.
    pub fn value(self) -> f32 {
        match self.rank {
            King => panic!("king has no defined value"),
            r => RANK_VALUES[r as usize],
        }
    }
}

impl Board {
    /// Return the value of the board for the given side.
    pub fn value(&self, on_move: Color) -> PosnValue {
        let mut cur_value = 0.0;
        let mut found_kings = [false, false];
        for r in 0..NROWS {
            for c in 0..NCOLS {
                match self.squares[r][c] {
                    Empty => (),
                    Full(piece) if piece.rank == King => {
                        let i = piece.color as usize;
                        assert!(!found_kings[i]);
                        found_kings[i] = true;
                    }
                    Full(piece) => {
                        let pv = piece.value();
                        if piece.color == on_move {
                            cur_value += pv
                        } else {
                            cur_value -= pv
                        }
                    }
                }
            }
        }
        assert!(found_kings[0] || found_kings[1]);
        for kc in [White, Black].iter() {
            if !found_kings[*kc as usize] {
                if on_move == *kc {
                    return Loss(0);
                } else {
                    return Win(0);
                }
            }
        }
        Playing(cur_value)
    }
}

impl Posn {
    /// Fully evaluate position for side on move.
    pub fn value(&self) -> PosnValue {
        match self.board.value(self.on_move) {
            Playing(v) => {
                if self.move_num > GAME_LENGTH {
                    Draw
                } else {
                    Playing(v)
                }
            }
            v => v,
        }
    }
}

impl PosnValue {
    /// This hard-to-name and hard-to-describe routine
    /// returns the adjustment to the position value
    /// from a given move.
    pub fn adjustment(dest: Square, old_source: Square, old_dest: Square) -> f32 {
        fn square_value(s: Square) -> f32 {
            match s {
                Empty => 0.0,
                Full(piece) => {
                    if piece.rank == King {
                        0.0
                    } else {
                        piece.value()
                    }
                }
            }
        }

        if let Some(King) = old_dest.rank() {
            panic!("adjust in winning position")
        };
        let odv = square_value(old_dest);
        let osv = square_value(old_source);
        let dv = square_value(dest);
        dv - osv + odv
    }

    /// This routine negates a position value in all the cases.
    pub fn negate(self) -> Self {
        match self {
            Playing(s) => Playing(-s),
            Win(n) => Loss(n),
            Loss(n) => Win(n),
            Draw => Draw,
        }
    }

    /// Indicate whether the given value is final or
    /// still playing.
    pub fn terminal(self) -> bool {
        !matches!(self, Playing(_))
    }
}
