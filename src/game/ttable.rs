// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

//! MiniChess transposition tables.
//!
//! This ttable implementation uses Zobrist keying and
//! random replacement. Entries are compared only by 64-bit Zobrist key,
//! so aliasing is theoretically possible.
//!
//! Final positions are always saved. Otherwise: searches of
//! depth 2 or less are not stored in the table;
//! shallower-searched entries will never replace
//! deeper-searched ones in the table; beta cutoff entries
//! of same global depth with narrower beta window will
//! never replace ones with wider window.

use crate::eval::*;
#[cfg(test)]
use crate::moves::*;
use crate::posn::*;
#[cfg(test)]
use crate::zobrist;

/// Default ttable size: see `TTable::new()` for details.
/// XXX Cannot be bigger than 2**31 (2 billion entries)
/// because Rust doesn't allow indexing a `Vector` with
/// `u64` and 2**32 does not fit in a `u32`.
pub static mut TTABLE_LG_SIZE: u32 = 0;

/// A ttable entry. XXX This could be compacted a lot.
#[derive(Clone, Copy)]
#[allow(clippy::upper_case_acronyms)]
pub struct TTEntry {
    /// Zobrist hash of the position represented.
    pub hash: u64,
    /// Value of the position.
    pub value: PosnValue,
    /// Beta cutoff at which the position was searched.
    pub beta: PosnValue,
    /// Depth of the search in plies from the current node.
    pub depth: usize,
    /// Ply at which the current node was saved, that is, "1 W" == ply 0,
    /// "1 B" == ply 1, etc.
    pub start_depth: usize,
}

/// A ttable.
pub struct TTable {
    /// Actual TTable entries, if hit.
    contents: Vec<Option<TTEntry>>,
}

impl TTable {
    /// Create a new ttable of default size.
    pub fn empty() -> TTable {
        let size = 1 << unsafe { TTABLE_LG_SIZE };
        let mut contents = Vec::with_capacity(size);
        contents.resize(size, None);
        TTable { contents }
    }

    pub fn boxed() -> Option<Box<TTable>> {
        if unsafe { TTABLE_LG_SIZE } > 0 {
            Some(Box::new(TTable::empty()))
        } else {
            None
        }
    }

    fn compute_index(&self, posn: &Posn) -> usize {
        let mask = (self.contents.len() - 1) as usize;
        posn.hash as usize & mask
    }

    /// Insert a state in the ttable, with all the trimmings.
    pub fn insert(
        &mut self,
        posn: &Posn,
        value: PosnValue,
        alpha: PosnValue,
        beta: PosnValue,
        depth: usize,
    ) {
        // Don't save underestimates, because it is dangerous.
        if value < alpha {
            return;
        }
        let i = self.compute_index(posn);
        let start_depth = posn.ply();
        // Always save final positions.
        if !value.terminal() {
            if depth <= 2 {
                return;
            };
            if let Some(ttentry) = self.contents[i] {
                // Never overwrite final positions. Always
                // overwrite hash mismatches.
                if !ttentry.value.terminal() && ttentry.hash == posn.hash {
                    // Don't save values that reach less far than
                    // what we already have.
                    if ttentry.depth > depth {
                        return;
                    };
                    // Don't save values that disimprove the beta window.
                    if ttentry.depth == depth
                        && ttentry.value >= ttentry.beta
                        && value >= beta
                        && ttentry.beta > beta
                    {
                        return;
                    };
                }
            };
        };
        let ttentry = TTEntry {
            hash: posn.hash,
            value,
            beta,
            depth,
            start_depth,
        };
        self.contents[i] = Some(ttentry);
    }

    /// If posn is in the ttable, return its info.
    pub fn lookup(
        &self,
        posn: &Posn,
        beta: PosnValue,
        depth: usize,
        any_hit: bool,
    ) -> Option<PosnValue> {
        let i = self.compute_index(posn);
        self.contents[i].as_ref().and_then(|ttentry| {
            if ttentry.hash != posn.hash {
                None
            } else if any_hit {
                Some(ttentry.value)
            } else if ttentry.depth < depth {
                None
            } else if ttentry.value < ttentry.beta || ttentry.value >= beta {
                Some(ttentry.value)
            } else {
                None
            }
        })
    }

    #[cfg(test)]
    fn delete(&mut self, posn: &Posn) {
        let i = self.compute_index(posn);
        self.contents[i] = None;
    }
}

// Just a basic sanity check on a single position.
#[test]
fn check_ttable_ops() {
    zobrist::fill_table();
    let posn_str = "1 W\n\
       kqbnr\n\
       pppPp\n\
       .....\n\
       .....\n\
       PPP.P\n\
       RNBQK\n";
    let mut posn: Posn = posn_str.parse().expect("invalid test position");
    unsafe { TTABLE_LG_SIZE = 20 };
    let mut ttable = TTable::empty();
    // Some values.
    let alpha = Playing(-3.0);
    let bogus_value = Playing(0.0);
    let bogus_result = Some(bogus_value);
    let larger_beta = Playing(1.0);
    let smaller_beta = Playing(-1.0);
    // Set up the position with an open window.
    ttable.insert(&posn, bogus_value, alpha, larger_beta, 9);
    // If everything is in-bounds, we're good.
    let result = ttable.lookup(&posn, Win(0), 8, false);
    assert!(result == bogus_result);
    // If lookup beta is small, we're good.
    let result = ttable.lookup(&posn, Loss(0), 8, false);
    assert!(result == bogus_result);
    // If the depth is too shallow, no good.
    let result = ttable.lookup(&posn, Loss(0), 10, false);
    assert!(result == None);
    // If the depth is too shallow but we'll take anything, we're good.
    let result = ttable.lookup(&posn, Loss(0), 10, true);
    assert!(result == bogus_result);
    // Set up the position as barely pruned.
    ttable.delete(&posn);
    ttable.insert(&posn, bogus_value, alpha, bogus_value, 9);
    // If the lookup window is low enough, we're good.
    let result = ttable.lookup(&posn, smaller_beta, 8, false);
    assert!(result == bogus_result);
    // If the lookup window is too high, no good.
    let result = ttable.lookup(&posn, larger_beta, 8, false);
    assert!(result == None);
    // Add a successor position.
    let capture: Move = "d5-e6".parse().expect("invalid test move");
    let _ = posn.make_move(&capture);
    // We should always miss on this missing position.
    let result = ttable.lookup(&posn, Loss(0), 1, true);
    assert!(result == None);
}
