// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Display macros with controlling variable.

/// If true, display game progress.
pub static mut DISPLAY_GAME: bool = true;

/// Provide suppressable display to stdout.
macro_rules! display {
    ($($a:expr),*) => (if unsafe {DISPLAY_GAME} {print!($($a),*);})
}
macro_rules! displayln {
    ($($a:expr),*) => (if unsafe {DISPLAY_GAME} {println!($($a),*);})
}
