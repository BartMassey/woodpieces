// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Global random numbers. Terrible LCG PRNG because Rust
//! and global state.
//! <http://nuclear.llnl.gov/CNP/rng/rngman/node4.html>
//! XXX Not at all thread safe.
//! XXX Fast, not crypto-secure.

extern crate rand;

static mut STATE: u64 = 0x123456789abcdef0u64;

/// Produce a 48-bit pseudo-random integer.
fn random() -> u64 {
    let state = unsafe { STATE };
    let tmp = state.wrapping_mul(2862933555777941757u64) + 3037000493u64;
    unsafe { STATE = tmp };
    tmp >> 16
}

/// Do stupid statistical tests on given function.
/// XXX This could fail probabilistically if the generator
/// was seeded randomly, so we pick a known-to-pass seed.
#[cfg(test)]
fn sample_test(f: fn() -> u64, bits: usize) {
    let nsamples = 1000000;
    unsafe { STATE = 0xe23456789abcd1f0u64 };
    let mut max_seen = f();
    let mut min_seen = max_seen;
    let mut sum: f64 = max_seen as f64;
    for _ in 1..nsamples {
        let v = f();
        if v > max_seen {
            max_seen = v
        };
        if v < min_seen {
            min_seen = v
        };
        sum += v as f64;
    }
    // Set up the constants.
    let nsamples = nsamples as f64;
    let range = (bits as f64).exp2();
    // XXX The deviation range for statistics is given by
    // probability calculations I am too lazy to reproduce
    // here, and then fudged by squaring to get the tests
    // to pass. Oh well.
    let p = 0.001f64;
    let dev = (range * (1.0 - (1.0 - p).powf(1.0 / nsamples))).powf(2.0);
    let expected_mean = (range - 1.0) / 2.0;
    let expected_min = dev;
    let expected_max = range - dev;
    // Sample statistics.
    let min_seen = min_seen as f64;
    let max_seen = max_seen as f64;
    let actual_mean = sum / nsamples;
    println!("range {:2.3e}  dev {:2.3e}", range, dev);
    println!(
        "dmean {:2.3e}  dmin {:2.3e}  dmax {:2.3e}",
        actual_mean - expected_mean,
        min_seen,
        range - max_seen
    );
    assert!((actual_mean - expected_mean).abs() <= 2.0 * dev);
    assert!(min_seen <= expected_min);
    assert!(max_seen >= expected_max);
}

/// Test random().
#[test]
fn sample_random() {
    sample_test(random, 48);
}

/// Return a white 64-bit random number.
pub fn random64() -> u64 {
    (random() << 32) | (random() >> 32)
}

/// Test random64().
#[test]
fn sample_random64() {
    sample_test(random64, 64);
}

/// Set the PRNG seed to a known value.
#[allow(dead_code)]
pub fn srandom(seed: u64) {
    unsafe { STATE = seed }
}

/// Set the PRNG seed to a system-derived value.
#[allow(dead_code)]
pub fn seed() {
    unsafe { STATE = rand::random() };
}

/// Return a random number in the range [0..bound).
/// Bound should be small (less than 16 bits).
pub fn randint(bound: usize) -> usize {
    self::random() as usize % bound
}

/// Shuffle the given vector. We do it faster than the
/// "good" one using standard Fisher-Yates.  Vec should be
/// smallish (less than 16 bits length).
pub fn shuffle<T: Copy>(vec: &mut Vec<T>) {
    let nvec = vec.len();
    for i in 0..nvec - 1 {
        let j = i + randint(nvec - i);
        vec.swap(i, j);
    }
}

/// Weak deterministic test of shuffling.  Just verifies
/// that sorting a shuffle returns the original array.
#[test]
fn shuffle_test() {
    let mut ints = Vec::new();
    for i in 0..100 {
        ints.push(i as u16);
    }
    let mut shuffled = ints.clone();
    shuffle(&mut shuffled);
    shuffled.sort();
    assert!(ints == shuffled);
}
