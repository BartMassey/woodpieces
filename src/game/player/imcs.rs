// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! IMCS "player". See
//! <http://codereview.stackexchange.com/questions/110073/>
//! from which this code liberally borrows.

use crate::board::*;
use crate::eval::*;
use crate::moves::*;
use crate::player::*;
use crate::posn::*;

use std::io::prelude::*;
use std::io::{stderr, BufReader};
use std::net::TcpStream;
use std::process::exit;
use std::str::FromStr;

extern crate encoding;
use self::encoding::all::ASCII;
use self::encoding::{EncoderTrap, Encoding};

/// Provide display of server info to stderr.
macro_rules! report {
    ($($a:expr),*) => (#[allow(clippy::explicit_write)] writeln!(stderr(), $($a),*).unwrap())
}

/// Provide display of server failure to stderr, then exit.
macro_rules! report_fail {
    ($($a:expr),*) => ({#[allow(clippy::explicit_write)] writeln!(stderr(), $($a),*).unwrap(); exit(1);})
}

/// Connection management.
struct Connection {
    /// Direct server stream.
    socket: TcpStream,
    /// Buffered reader stream.
    reader: BufReader<TcpStream>,
}

impl Connection {
    /// Create a new server connection.
    pub fn new(host: &str, port: &str) -> Self {
        let sa: &str = &format!("{}:{}", host, port);
        let server = TcpStream::connect(sa).expect("could not connect to server");
        // Disable Nagleing.
        server.set_nodelay(true).expect("could not set nodelay");
        let read_server = server.try_clone().expect("could not clone socket");
        let reader = BufReader::new(read_server);
        Connection {
            socket: server,
            reader,
        }
    }

    fn send(&mut self, cmd: &str) {
        report!("-> {}", cmd);
        let cmdstr: &str = &format!("{}\r\n", cmd);
        let cmdbytes = ASCII
            .encode(cmdstr, EncoderTrap::Strict)
            .expect("could not encode command");
        self.socket
            .write_all(&cmdbytes)
            .expect("could not send command");
        self.socket.flush().expect("could not flush command");
    }

    fn get_line(&mut self, report: bool) -> String {
        let mut line = String::new();
        let _ = self
            .reader
            .read_line(&mut line)
            .expect("could not read line from server");
        let result = line.trim_end();
        if report {
            report!("<- {}", result);
        }
        result.to_string()
    }

    fn get_nonempty_line(&mut self, report: bool) -> String {
        loop {
            let next_line = self.get_line(report);
            if !next_line.is_empty() {
                return next_line;
            }
        }
    }

    fn expect(&mut self, expect_code: &str) -> Option<String> {
        let response = self.get_line(true);
        if &response[3..4] != " " {
            panic!("invalid response: {}", response);
        };
        let code = &response[0..3];
        for c in code.chars() {
            if c.is_ascii_digit() {
                panic!("invalid code: {}", code);
            }
        }
        if code == expect_code {
            Some(response[4..].to_string())
        } else {
            None
        }
    }

    /// Create and initialize a new player.
    pub fn connect(username: &str, password: &str, host: &str, port: &str) -> Self {
        let mut server = Connection::new(host, port);
        let version = server.expect("100").unwrap();
        if version != "imcs 2.5" {
            panic!("bad version: ({})", version);
        };
        server.send(&format!("me {} {}", username, password));
        let _ = server.expect("201").unwrap();
        server
    }
}

/// The IMCS player caches its state.
#[allow(clippy::upper_case_acronyms)]
pub struct IMCSPlayer {
    /// Connection to remote server.
    server: Connection,
    /// Color being played by this program. This is the
    /// color we turn up on the server as, and the opposite
    /// of the color that we were invoked as.
    color: Color,
}

fn start_code(color: Color) -> &'static str {
    match color {
        White => "105",
        Black => "106",
    }
}

impl IMCSPlayer {
    /// Accept a game on the connection
    pub fn accept(
        username: &str,
        password: &str,
        host: &str,
        port: &str,
        color: Color,
        game_id: &str,
    ) -> Self {
        let mut server = Connection::connect(username, password, host, port);
        server.send(&format!("accept {}", game_id));
        let _ = server.expect(start_code(color)).expect("accept failed");
        IMCSPlayer { server, color }
    }

    /// Offer a game on the connection
    pub fn offer(username: &str, password: &str, host: &str, port: &str, color: Color) -> Self {
        let mut server = Connection::connect(username, password, host, port);
        server.send(&format!("offer {}", color));
        let offer = server.expect("103").expect("unexpected offer response");
        let game_id = offer
            .split(' ')
            .next()
            .expect("unexpected offer response format");
        report!("offering {} game {}", color, game_id);
        let _ = server
            .expect(start_code(color))
            .expect("unexpected game start response");
        IMCSPlayer { server, color }
    }

    /// Read a position from the server and return it.
    fn read_posn(&mut self) -> Posn {
        let mut posn_strs = Vec::new();
        for _ in 0..NROWS + 1 {
            let line = self.server.get_nonempty_line(false);
            posn_strs.push(line);
        }
        let _ = self.server.get_line(false);
        posn_strs.push("".to_string());
        let posn_str: &str = &posn_strs.join("\n");
        match FromStr::from_str(posn_str) {
            Ok(p) => p,
            Err(_) => report_fail!("bad position description:\n{}", posn_str),
        }
    }
}

impl Player for IMCSPlayer {
    /// Obtain a move from the opponent.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult {
        assert!(self.color == posn.on_move.opponent());
        let resp = self.server.get_nonempty_line(true);
        let resp_words: Vec<&str> = resp.split(' ').collect();
        let nwords = resp_words.len();
        assert!(nwords > 0);
        match resp_words[0] {
            "!" => {
                if nwords != 2 {
                    report_fail!("malformed move report {}", resp)
                };
                if let Ok(opp_move) = FromStr::from_str(resp_words[1]) {
                    NextMove(opp_move, None)
                } else {
                    report_fail!("malformed move {}", resp)
                }
            }
            "=" => {
                if nwords < 2 {
                    report_fail!("malformed result report {}", resp)
                };
                match resp_words[1] {
                    "draw" => {
                        let _ = self.server.expect("230");
                        GameOver(Draw)
                    }
                    "W" => {
                        let _ = self.server.expect("231");
                        match self.color {
                            White => GameOver(Loss(0)),
                            Black => GameOver(Win(0)),
                        }
                    }
                    "B" => {
                        let _ = self.server.expect("232");
                        match self.color {
                            White => GameOver(Win(0)),
                            Black => GameOver(Loss(0)),
                        }
                    }
                    _ => report_fail!("unknown result report {}", resp),
                }
            }
            _ => report_fail!("malformed server response {}", resp),
        }
    }

    fn report_move(&mut self, cpu_posn: &Posn, cpu_move: Move) {
        assert!(self.color == cpu_posn.on_move);
        let posn = self.read_posn();
        if posn != *cpu_posn {
            report_fail!("position mismatch")
        };
        let req = self.server.get_nonempty_line(true);
        let req_words: Vec<&str> = req.split(' ').collect();
        if req_words.len() != 3 || req_words[0] != "?" {
            report_fail!("bad request")
        }
        self.server.send(&format!("{}", cpu_move));
    }
}
