// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Iterative Deepening player. Uses a Negamax player
//! for the actual negamax.

use crate::board::*;
use crate::display::*;
use crate::eval::*;
use crate::player::*;
use crate::posn::*;
use crate::ttable::*;

use std::time::*;

/// ID player state.
#[allow(clippy::upper_case_acronyms)]
pub struct IDPlayer {
    /// Time for each move. If this is not specified,
    /// a heuristic will be used at each move.
    search_duration: Option<Duration>,
    /// The player under the hood plays negamax.
    negamax_player: NegamaxPlayer,
}

impl IDPlayer {
    /// Create a new negamax player.
    pub fn new(
        search_time: Option<f32>,
        ab: bool,
        color: Color,
        ttable: Option<Box<TTable>>,
    ) -> Self {
        // XXX 1 is arbitrary here. Should be overwritten by ID.
        let player = NegamaxPlayer::new(1, ab, color, ttable);
        let search_duration = search_time.map(|secs| {
            let ms = (secs * 1000.0).ceil() as u64;
            Duration::from_millis(ms)
        });
        IDPlayer {
            search_duration,
            negamax_player: player,
        }
    }
}

impl Player for IDPlayer {
    /// Select a move based on negamax value at target depth.
    /// Expects to be called in an ongoing position.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult {
        let search_duration = self
            .search_duration
            .unwrap_or_else(|| panic!("no search time heuristic yet"));
        self.negamax_player.search_timer = Some((Instant::now(), search_duration));
        // Iterative-deepening.
        let mut depth = 1;
        let mut last_result = None;
        loop {
            self.negamax_player.search_depth = depth;
            let negamax_result = self.negamax_player.negamax(posn, depth, (Loss(0), Win(0)));
            let (m, v) = match negamax_result {
                NegamaxMove(None, Loss(0)) => {
                    assert!(depth == 1);
                    return GameOver(Loss(0));
                }
                NegamaxMove(None, v) => panic!("id no move with value {}", v),
                NegamaxMove(Some(m), v) => (m, v),
                NegamaxValue(_) => panic!("id failed to choose a move"),
                NegamaxTimeout => {
                    assert!(depth > 1);
                    return last_result.expect("id timeout with no move");
                }
            };
            let result = Some(NextMove(m, Some(v)));
            displayln!("depth {}: {} ({})", depth, m, v);
            // If we're done, we're done.
            if v.terminal() {
                return result.expect("id stopped with no move");
            };
            last_result = result;
            depth += 1;
        }
    }
}
