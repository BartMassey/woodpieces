// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Negamax player.

use crate::board::*;
use crate::eval::*;
use crate::moves::*;
use crate::myrng;
use crate::player::*;
use crate::posn::*;
use crate::ttable::*;

use std::cmp::*;
use std::time::*;

/// How many nodes to search before checking the timer again
/// during ID.
const TIMER_CHECK_INTERVAL: usize = 10000;

// If true, ensure that ab-pruned result is same as unpruned
// at each search step.
pub static mut CHECK_AB: bool = false;

// If true, shuffle the move list in negamax;
pub static mut SHUFFLE_MOVES: bool = false;

// If true, shuffle the top-level move list in negamax;
pub static mut SHUFFLE_TOP: bool = true;

/// Negamax player state.
pub struct NegamaxPlayer {
    /// Search depth.
    pub search_depth: usize,
    /// Use alpha-beta pruning.
    pub ab: bool,
    /// Ensure select_move() is called only with positions
    /// of correct color.
    pub color: Color,
    /// Check ab pruning if requested.
    pub check_ab: bool,
    /// Node count of current search.
    pub nodes_searched: usize,
    /// Start time and duration of current search, if ID.
    pub search_timer: Option<(Instant, Duration)>,
    /// Transposition table if desired.
    pub ttable: Option<Box<TTable>>,
}

/// Negamax returns a move at top-level and a position value
/// everywhere else.
#[derive(Clone, Copy)]
pub enum NegamaxResult {
    /// Value of the position.
    NegamaxValue(PosnValue),
    /// Some best move in position, or None if
    /// there are no moves.
    NegamaxMove(Option<Move>, PosnValue),
    /// Search timed out in ID.
    NegamaxTimeout,
}

impl NegamaxPlayer {
    /// Create a new negamax player.
    pub fn new(search_depth: usize, ab: bool, color: Color, ttable: Option<Box<TTable>>) -> Self {
        NegamaxPlayer {
            search_depth,
            ab,
            color,
            check_ab: unsafe { CHECK_AB },
            nodes_searched: 0,
            search_timer: None,
            ttable,
        }
    }

    /// Return the negamax value of the given position,
    /// pruned at the given depth.
    pub fn negamax(
        &mut self,
        posn: &mut Posn,
        depth: usize,
        (alpha, beta): (PosnValue, PosnValue),
    ) -> NegamaxResult {
        self.nodes_searched += 1;
        // Check for timeout.
        #[allow(clippy::collapsible_if)]
        if let Some((start_time, search_duration)) = self.search_timer {
            if self.nodes_searched % TIMER_CHECK_INTERVAL == 0 {
                if start_time.elapsed() >= search_duration {
                    return NegamaxTimeout;
                }
            }
        };
        // The top level is "special".
        assert!(depth <= self.search_depth);
        let top_level = depth == self.search_depth;
        // TTable prune. Note that a top-level prune is
        // not useful, because there is no move there.
        if !top_level {
            if let Some(ref ttable) = self.ttable {
                if let Some(value) = ttable.lookup(posn, beta, depth, false) {
                    return NegamaxValue(value);
                }
            }
        };
        // Depth prune.
        if depth == 0 {
            assert!(!top_level);
            return NegamaxValue(posn.posn_value);
        };
        // Check ab if required.
        if self.ab && self.check_ab {
            let mut player = NegamaxPlayer {
                search_depth: self.search_depth,
                ab: true,
                color: self.color,
                check_ab: false,
                nodes_searched: 0,
                search_timer: None,
                ttable: None,
            };
            player.check_ab = false;
            player.search_timer = None;
            let abv = match player.negamax(posn, depth, (alpha, beta)) {
                NegamaxMove(_, v) => v,
                NegamaxValue(v) => v,
                NegamaxTimeout => panic!(
                    "unexpected timeout in \
                                              check_ab abv"
                ),
            };
            player.ab = false;
            let nmv = match player.negamax(posn, depth, (Loss(0), Win(0))) {
                NegamaxMove(_, v) => v,
                NegamaxValue(v) => v,
                NegamaxTimeout => panic!(
                    "unexpected timeout in \
                                              check_ab nmv"
                ),
            };
            if abv != nmv && (abv < beta || abv > nmv) && (abv >= alpha || abv < nmv) {
                panic!(
                    "check_ab: d {}  abv {}  nmv {}  \
                       alpha {}  beta {}\n{}",
                    depth, abv, nmv, alpha, beta, posn
                );
            };
        };
        // Generate the moves, losing if there aren't any.
        let mut moves = posn.movegen();
        let nmoves = moves.len();
        if nmoves == 0 {
            if let Some(ref mut ttable) = self.ttable {
                ttable.insert(posn, Loss(0), alpha, beta, depth)
            };
            if top_level {
                return NegamaxMove(None, Loss(0));
            } else {
                return NegamaxValue(Loss(0));
            };
        };
        // Maybe shuffle the moves.
        if (unsafe { SHUFFLE_TOP } && top_level) || (unsafe { SHUFFLE_MOVES } && self.ab) {
            myrng::shuffle(&mut moves);
        };
        // Order ttable moves first by decreasing score.
        if let Some(ref ttable) = self.ttable {
            if depth >= 2 {
                let mut move_values = Vec::with_capacity(moves.len());
                for ref m in moves {
                    let (_, undo) = posn.make_move(m);
                    let ttval = ttable.lookup(posn, beta, depth, true);
                    posn.unmake_move(m, &undo);
                    move_values.push((ttval, *m))
                }
                // Sort move-values in reverse order by value only.
                move_values.sort_by(|a, b| b.0.cmp(&a.0));
                // Keep just the moves.
                moves = move_values.iter().map(|mv| mv.1).collect();
            }
        };
        // The actual search.
        let mut best_value = Loss(0);
        let mut best_move = None;
        for ref m in moves {
            let (posn_value, undo) = posn.make_move(m);
            let v = match posn_value {
                Playing(_) => {
                    let alpha_m = best_value.negate();
                    let beta_m = beta.negate();
                    let v_opp = self.negamax(posn, depth - 1, (beta_m, alpha_m));
                    match v_opp {
                        NegamaxValue(v) => v.negate(),
                        NegamaxTimeout => {
                            posn.unmake_move(m, &undo);
                            return NegamaxTimeout;
                        }
                        _ => panic!("negamax returned unexpected result"),
                    }
                }
                final_value => final_value,
            };
            posn.unmake_move(m, &undo);
            if best_value == Loss(0) || v > best_value {
                best_value = v;
                if top_level {
                    best_move = Some(*m);
                };
                if self.ab && best_value >= beta {
                    break;
                };
            };
        }
        // Adjust length of win or loss.
        let best_value = match best_value {
            Win(n) => Win(n + 1),
            Loss(n) => Loss(n + 1),
            v => v,
        };
        // Save result in ttable.
        if let Some(ref mut ttable) = self.ttable {
            ttable.insert(posn, best_value, alpha, beta, depth)
        };
        // Report.
        if top_level {
            assert!(best_move != None);
            NegamaxMove(best_move, best_value)
        } else {
            NegamaxValue(best_value)
        }
    }
}

impl Player for NegamaxPlayer {
    // Select a move based on negamax value at target depth.
    // Expects to be called in an ongoing position.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult {
        assert!(posn.on_move == self.color);
        let depth = self.search_depth;
        match self.negamax(posn, depth, (Loss(0), Win(0))) {
            NegamaxMove(None, Playing(_)) => panic!("game over in nonterminal position"),
            NegamaxMove(None, v) => GameOver(v),
            NegamaxMove(Some(m), v) => NextMove(m, Some(v)),
            NegamaxValue(_) => panic!("failed to choose a move"),
            NegamaxTimeout => panic!("negamax_player unexpected timeout"),
        }
    }
}
