// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Human "player".

use std::io::{stdin, stdout, Write};

use crate::eval::*;
use crate::player::*;
use crate::posn::*;

/// The human player has no state.
#[derive(Default)]
pub struct HumanPlayer {}

impl HumanPlayer {
    /// Create a new human player.
    pub fn new() -> Self {
        Self::default()
    }
}

impl Player for HumanPlayer {
    /// Humanly select a move.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult {
        // Generate the moves, losing if there aren't any.
        let moves = posn.movegen();
        let nmoves = moves.len();
        if nmoves == 0 {
            return GameOver(Loss(0));
        };
        // Prompt for a move until a legal one is obtained,
        // then return it.
        loop {
            print!("> ");
            stdout().flush().unwrap();
            let mut move_string = String::new();
            stdin()
                .read_line(&mut move_string)
                .expect("could not read from input");
            let move_str = move_string.trim();
            if move_str == "resign" {
                return GameOver(Loss(0));
            };
            if move_str == "moves" {
                for m in &moves {
                    println!("{}", m);
                }
                println!();
                println!("{}", posn);
                continue;
            };
            for m in &moves {
                if m.to_string() == move_str {
                    return NextMove(*m, None);
                }
            }
            println!("illegal or invalid move");
            println!();
            println!("{}", posn);
        }
    }
}
