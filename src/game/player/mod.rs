// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Player interface.

/// The players.
mod human;
mod id;
mod imcs;
mod negamax;
mod random;

pub use self::human::*;
pub use self::id::*;
pub use self::imcs::*;
pub use self::negamax::NegamaxResult::*;
pub use self::negamax::*;
pub use self::random::*;

use crate::eval::*;
use crate::moves::*;
use crate::posn::*;

/// Things a player can say.
pub enum MoveResult {
    /// A move has been selected. If it has
    /// an evaluation, indicate it.
    NextMove(Move, Option<PosnValue>),
    /// The game is over.
    GameOver(PosnValue),
}
pub use self::MoveResult::*;

/// A game player selects moves and maybe ships them
/// externally.
pub trait Player {
    /// Given a position, pick a move according to a
    /// criterion.  Leave the position how it was found.
    /// Return the selected move, or `None` if no move is
    /// available. The position should be in progress.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult;

    /// Send a move to an external player. By default, this
    /// does nothing.
    fn report_move(&mut self, _: &Posn, _: Move) {}
}
