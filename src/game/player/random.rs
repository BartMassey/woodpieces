// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Random player.

use crate::eval::*;
use crate::myrng::*;
use crate::player::*;
use crate::posn::*;

/// The random player has no state.
#[derive(Default)]
pub struct RandomPlayer {}

impl RandomPlayer {
    /// Create a new random player.
    pub fn new() -> Self {
        Self::default()
    }
}

impl Player for RandomPlayer {
    /// Randomly select a move.
    fn select_move(&mut self, posn: &mut Posn) -> MoveResult {
        // Generate the moves, losing if there aren't any.
        let moves = posn.movegen();
        let nmoves = moves.len();
        if nmoves == 0 {
            return GameOver(Loss(0));
        };
        // Select a random move.
        let move_number = randint(nmoves);
        let chosen_move = moves[move_number];
        // Report.
        NextMove(chosen_move, None)
    }
}
