// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

//! MiniChess Zobrist hashing.

use crate::board::*;
use crate::moves::*;
use crate::myrng;
use crate::posn::*;

/// A Zobrist table is a big table of random numbers used for hashing a board.
type ZobristTable = [[[[u64; NCOLORS]; NRANKS]; NCOLS]; NROWS];
static mut ZOBRIST_TABLE: ZobristTable = [[[[0; NCOLORS]; NRANKS]; NCOLS]; NROWS];

/// We also need a Zobrist key for hashing a position, which includes side-on-move.
static mut ZOBRIST_SIDE_BLACK: u64 = 0;

/// Fill the Zobrist key table. This needs to be done before
/// using any of the methods: usually at the beginning of
/// the program. XXX This is not checked.
pub fn fill_table() {
    #![allow(clippy::needless_range_loop)]
    for r in 0..NROWS {
        for c in 0..NCOLS {
            for p in 0..NRANKS {
                for s in 0..NCOLORS {
                    let v = myrng::random64();
                    unsafe { ZOBRIST_TABLE[r][c][p][s] = v };
                }
            }
        }
    }
    let v = myrng::random64();
    unsafe { ZOBRIST_SIDE_BLACK = v };
}

impl Board {
    /// Compute the Zobrist hash of a board.
    pub fn zobrist_hash(&self) -> u64 {
        #![allow(clippy::needless_range_loop)]
        let mut h = 0u64;
        for r in 0..NROWS {
            for c in 0..NCOLS {
                match self.squares[r][c] {
                    Empty => (),
                    Full(piece) => {
                        let p = piece.rank as usize;
                        let s = piece.color as usize;
                        h ^= unsafe { ZOBRIST_TABLE[r][c][p][s] }
                    }
                }
            }
        }
        h
    }

    /// Adjust the given Zobrist hash to reflect a move on the board.
    pub fn zobrist_move_hash(&self, h: u64, old_loc: &Loc, new_loc: &Loc, dest: Square) -> u64 {
        let z_piece = |loc: &Loc, piece: Piece| {
            let p = piece.rank as usize;
            let s = piece.color as usize;
            unsafe { ZOBRIST_TABLE[loc.r][loc.c][p][s] }
        };
        let z_square = |loc: &Loc| match self.squares[loc.r][loc.c] {
            Empty => 0,
            Full(piece) => z_piece(loc, piece),
        };
        let old_source_hash = z_square(old_loc);
        let new_source_hash = z_square(new_loc);
        let new_dest_hash = if let Full(dest_piece) = dest {
            z_piece(new_loc, dest_piece)
        } else {
            panic!("no ending piece for move hashing")
        };
        h ^ old_source_hash ^ new_source_hash ^ new_dest_hash
    }
}

impl Posn {
    /// Compute the Zobrist hash of a position.
    pub fn zobrist_hash(&self) -> u64 {
        let h = self.board.zobrist_hash();
        match self.on_move {
            White => h,
            Black => h ^ unsafe { ZOBRIST_SIDE_BLACK },
        }
    }

    /// Adjust the given Zobrist hash to reflect a change of side.
    pub fn zobrist_side_change_hash(&self, h: u64) -> u64 {
        h ^ unsafe { ZOBRIST_SIDE_BLACK }
    }
}

// Just a basic sanity check on a single position.
#[test]
fn check_incremental_hash() {
    fill_table();
    let posn_str = "1 W\n\
       kqbnr\n\
       pppPp\n\
       .....\n\
       .....\n\
       PPP.P\n\
       RNBQK\n";
    let mut posn: Posn = posn_str.parse().expect("invalid test position");
    let start_hash = posn.zobrist_hash();
    let capture: Move = "d5-e6".parse().expect("invalid test move");
    let white_queen = Full(Piece {
        rank: Queen,
        color: White,
    });
    let move_hash =
        posn.board
            .zobrist_move_hash(start_hash, &capture.start, &capture.end, white_queen);
    let end_hash = posn.zobrist_side_change_hash(move_hash);
    let _ = posn.make_move(&capture);
    let ref_hash = posn.zobrist_hash();
    assert!(end_hash == ref_hash);
    assert!(posn.hash == ref_hash);
}
