// Copyright © 2017 Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file COPYING in the source
// distribution of this software for license terms.

// Wood Pieces

//! Position (state) information.

use std::fmt;
use std::io::Read;
use std::str::{from_utf8, FromStr};

use crate::board::*;
use crate::eval::*;

/// Number of moves in a complete game.
pub const GAME_LENGTH: usize = 40;

/// Control the maintenance of Zobrist hashes for positions.
pub static mut USE_HASH: bool = true;

/// Position.
#[derive(PartialEq, Eq)]
pub struct Posn {
    pub board: Board,
    /// Move (not ply) counting from 1.
    pub move_num: usize,
    /// Side on move.
    pub on_move: Color,
    /// Value of position for side on move.
    pub posn_value: PosnValue,
    /// Zobrist hash of position.
    pub hash: u64,
}

impl fmt::Display for Posn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{} {}", self.move_num, self.on_move)?;
        write!(f, "{}", self.board)
    }
}

impl FromStr for Posn {
    type Err = DescriptionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Break up the position and deal with the info row.
        let mut rows = s.split('\n');
        let mut info = rows
            .next()
            .ok_or_else(|| ErrBadInfoLine(s.into()))?
            .split(' ');
        // Get the move number.
        let move_num = info
            .next()
            .ok_or(ErrMissingMoveNumber)?
            .parse()
            .map_err(ErrInvalidMoveNumber)?;
        // Get the side on move.
        let on_move = info.next().ok_or(ErrMissingOnMove)?.parse()?;
        // Turn the board back into a string and
        // make a `Board` out of it.
        let rows_strs: Vec<&str> = rows.collect();
        let board_str = rows_strs.join("\n");
        let board: Board = board_str.parse()?;
        Ok(Posn::new(on_move, move_num, &board))
    }
}

impl Posn {
    /// Fill out the "extra" fields of a position record.
    pub fn new(on_move: Color, move_num: usize, board: &Board) -> Posn {
        // Build the position.
        let mut posn = Posn {
            move_num,
            on_move,
            board: (*board).clone(),
            posn_value: Draw,
            hash: 0,
        };
        posn.posn_value = posn.value();
        posn.hash = if unsafe { USE_HASH } {
            posn.zobrist_hash()
        } else {
            0
        };
        // Return the completed position.
        posn
    }

    /// Return the initial position for an official game.
    pub fn initial() -> Posn {
        let board = Board::initial();
        Posn::new(White, 1, &board)
    }

    /// Return a position read from the specified file, or
    /// an error.
    pub fn read(f: &mut dyn Read) -> Result<Posn, DescriptionError> {
        let mut contents: Vec<u8> = Vec::new();
        f.read_to_end(&mut contents).map_err(ErrBadPosnFile)?;
        let posn_str = from_utf8(&contents).map_err(ErrBadPosnFileUtf8)?;
        posn_str.parse()
    }

    /// Return the absolute ply this position sits at in
    /// the overall game, counting from 0.
    pub fn ply(&self) -> usize {
        2 * (self.move_num - 1) + self.on_move as usize
    }
}
