# Building An Endgame Database
Bart Massey 2017-05-09

http://www.chesshistory.com/winter/extra/movesahead.html

## Endgame Play

* No competent chess player playing white would look at a
  K+Q vs k position and try to figure out what move to make
  next.

* But it's more complicated than that, of course. Two things
  to worry about:

  * Endgame principles
  * Winning known endgames

* "It is a well-known phenomenon that the same amateur who
  can conduct the middle game quite creditably, is usually
  perfectly helpless in the end game." --Aaron Nimzovitch


## When Does The Endgame Begin?

* When few pieces are left on the board.

* In Minichess, as move 40 approaches.

* When search depths get short (narrow "main line").

## Endgame Principles

* There is a lot of endgame-specific general advice on play.

* For our minichess player, we really don't have much of a
  way to implement that advice except to adjust the
  evaluation function in the endgame.

* Specific endgame evaluators focus on pawn capture and
  promotion. Especially in MiniChess.

* Limited.

## Provably-Correct Procedural Endgame Evaluation / Play

* Consider that K+Q vs k board state. A
  competent player playing white will seek this state,
  because it is a known win. A competent player playing
  black will avoid it, for the same reason.

* Complicated by draws on moves --- especially in
  MiniChess. Too few moves can turn a win or loss into a
  draw.

* Programming a player to correctly play the K+Q vs k ending
  is not that challenging, but it's a couple hours of
  work. Calculating the necessary win/loss length for a
  given position would be quite a bit harder.

* There are a bunch of endgames like this, though: for
  example B+N+K vs k. For Minichess, it is not known which
  of these are even wins.

## Provable Tablebase-Driven Endgame Play

* In most endgame situations the existence of a win depends
  on the position of the pieces and the number of moves
  remaining.

* Search is generally not able to look deep enough. In chess
  sometimes 10-15 moves (20-30 plys) for a forced win. In
  Minichess?

* Idea: Store every position with a small number of pieces
  in a big "tablebase". Annotate the positions with win,
  loss or draw; also with a shortest-win length or a
  longest-loss length.

* When the search hits this table, it can prune, as if it
  were a perfect transposition table.

## Size Of Endgame Tablebase

* It is likely infeasible to store all the possible
  positions even in Minichess. With all pieces on the board,
  a quick (and bad) estimate gives choose(30,20) \*
  (choose(10,5) \* 5!)^2 = 2.3x10^16 positions, or about 27
  exapositions.

* For K+R vs k, there are apparently choose(30,2) \* 28 =
  12180 positions to consider. This seems pretty darn
  reasonable. For K+R vs k+r we have 2 \* choose(30,2) \*
  choose(28, 2) = 164430 positions. You can see where this
  is going.

* Some tricks are possible.

  * Because zero-sum, only store positions with white on move.
  
  * Need not store drawn positions.

  * The positions can be made much smaller by compression.

## Retrograde Analysis: Computing the Endgame Tablebase

* We should start with the K vs k tablebase. For chess, this
  is trivial: all reachable positions are draws. Not quite
  in Minichess.

* Two basic approaches: "uncapturing" and "transitive closure".

* Uncapturing: Start with K by itself at every possible
  position: the K vs nobody tablebase. These positions are
  all wins :-). 
  
  The position must have been reached by K capturing k. So
  "unmove" each possible capture in each possible position,
  and thus get a set of K vs k positions that are a win for
  white in 1. Obviously, these are the positions with
  touching kings.

  Now, make each possible black unmove for each of these
  positions that does not involve a capture. These positions
  can be lost by black in 1, but...

## Retrograde Analysis: One For A Win, N For A Loss

* Uncapturing cont: You can't assume that black will make
  the losing move we found. Unless black has to. If we find
  a position where all legal moves from the unmove lead to a
  known loss, we mark the position as a loss. We flip the
  board and keep going.

  As losses get filled in, they get marked with longest
  length of any successor plus one. As wins get filled in,
  they are marked with shortest leaf of any successor plus one.
  (That will always be the number of unmoves so far.)

  When no more progress can be made, the remaining positions
  are draws (in any number of moves).

## Retrograde Analysis: Forward Transitive Closure

* Unmoves that uncapture pieces mean that the whole
  tablebase has to be built at once. This isn't really
  viable.

* For each possible K vs k position, try each possible K
  move. If any win, the position is a win in 1.
 
  Now iterate again. If a win in 2 is found, add it.

  Continue until no more progress is made.

* Slower: considers the same moves over and over. Easier to
  program. Allows some parallelism

* Can still use non-uncapturing unmoves to limit the search.

## Using the Endgame Tablebase

* Modify your negamax search to consult the endgame
  tablebase before the transposition table whenever you
  reach a position whose pieces are known to be in the
  tablebase. Remember to check the number of remaining moves
  for draws.

* Speed matters here: you will be doing this lookup a lot.
  You can use a hashed SSD store, but probably should load
  the tablebase into memory.

* Once the game position hits the tablebase, the player moves
  perfectly and instantly (that is, after a depth-1 search).

## Congratulations!

* Your Minichess player is no longer helpless in the
  endgame.

## Algorithm Details

Here is some pseudocode for the forward transitive closure
endgame tablebase builder.

> To&nbsp;build&nbsp;a&nbsp;tablebase&nbsp;*T*&nbsp;for&nbsp;a&nbsp;collection&nbsp;*P*&nbsp;of&nbsp;pieces,&nbsp;given&nbsp;tablebases  
> *T*[*P&#x2032;*]\[*S&#x2032;*]&nbsp;for&nbsp;all&nbsp;states&nbsp;*S&#x2032;*&nbsp;drawn&nbsp;from&nbsp;subcollections&nbsp;*P&#x2032;*&nbsp;of&nbsp;*P*&nbsp;such&nbsp;that  
> |*P&#x2032;*|&nbsp;=&nbsp;|*P*|&nbsp;-&nbsp;1:  
> &nbsp;&nbsp;&nbsp;&nbsp;*T*&nbsp;&#8592;&nbsp;empty  
> &nbsp;&nbsp;&nbsp;&nbsp;*open*&nbsp;&#8592;&nbsp;set&nbsp;of&nbsp;states&nbsp;corresponding&nbsp;to&nbsp;all&nbsp;arrangements  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of&nbsp;*P*&nbsp;on&nbsp;the&nbsp;board  
> &nbsp;&nbsp;&nbsp;&nbsp;*solved*&nbsp;&#8592;&nbsp;**false**  
> &nbsp;&nbsp;&nbsp;&nbsp;**while**&nbsp;**not**&nbsp;*solved*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*solved*&nbsp;&#8592;&nbsp;**true**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**for**&nbsp;*s*&nbsp;**in**&nbsp;*open*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*loss*&nbsp;&#8592;&nbsp;**true**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*max_loss*&nbsp;&#8592;&nbsp;0  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**for**&nbsp;*s&#x2032;*&nbsp;**in**&nbsp;*successors*(*s*)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**if**&nbsp;*s&#x2032;*&nbsp;**in**&nbsp;*T*&nbsp;**or**&nbsp;*pieces*(*s&#x2032;*)&nbsp;**in**&nbsp;some&nbsp;*P&#x2032;*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*v*&nbsp;&#8592;&nbsp;value&nbsp;of&nbsp;*s&#x2032;*&nbsp;from&nbsp;*T*&nbsp;or&nbsp;*P&#x2032;*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**if**&nbsp;*v*&nbsp;=&nbsp;*win*(*n*)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*loss*&nbsp;&#8592;&nbsp;**false**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*solved*&nbsp;&#8592;&nbsp;**false**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*T*[*s*]&nbsp;=&nbsp;*win*(*n*&nbsp;+&nbsp;1)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remove&nbsp;*s*&nbsp;from&nbsp;*open*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**break**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**else**&nbsp;**if**&nbsp;*v*&nbsp;=&nbsp;*loss*(*n*)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*max_loss*&nbsp;=&nbsp;**max**(*max_loss*,&nbsp;*n*+1)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**else**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*loss*&nbsp;&#8592;&nbsp;**false**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**if**&nbsp;*loss*  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*solved*&nbsp;&#8592;&nbsp;**false**  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*T*[*s*]&nbsp;=&nbsp;*loss*(*max_loss*)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remove&nbsp;*s*&nbsp;from&nbsp;*open*  
> &nbsp;&nbsp;&nbsp;&nbsp;**return**&nbsp;*T*  

This algorithm relies on the fact that the counts will be
updated properly. Because it is iterative, one cannot reach
a too-long win or a too-short loss "early". To do that, one
would have to consider a predecessor position too soon, so
induction on the number of cycles of the while loop
suffices.

Note that all the *n*-piece tables can be built in parallel
once the *n-1*-piece tables are built. This parallelism is
nice if you have lots of machines. Except...

There's a slight twist here. When a pawn promotes to a
queen, you still need to know the value of the position, so
you'll need to build the table with *n* extra queens before
building the table with *n* pawns. The obvious plan is to
build all the queens-no-pawns tables first, then
queens-one-pawn, etc.

Rather than searching all open positions on each iteration,
the open list is updated to contain only predecessors of
newly-solved positions. This requires the ability to do
non-uncapturing unmove, so some special code. It gives
a huge speedup, but makes the proof of correctness (not
supplied anyway) a little harder.

This algorithm needs a correctness proof. I think one is
straightforward to construct, but I don't have it handy
right now.  The basic outline is to first prove soundness by
showing that any position that makes it into the table is
marked correctly. Then prove completeness by showing that
when the algorithm stops, there cannot be any positions left
that could be solved.
