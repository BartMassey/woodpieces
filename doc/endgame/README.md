# Endgame Tablebases

This directory contains documentation for `endgame.rs`, the
Endgame Tablebase builder.

* `endgame.md` is a general description of the approach
  used to build tablebases.
  
* `build-tablebase.pseu` is pseudocode for the builder in
  [`pseuf`](http://github.com/BartMassey/pseuf) format.
